<?php

$args = array(
    'posts_per_page'   => -1,
    'offset'           => 0,
    'order'            => 'DESC',
    'post_type'        => 'banner',
    'post_status'      => 'publish',

);   

$banners = get_posts( $args );

?>
<div id="banner">
<?php foreach($banners as $banner): ?>

    <?php echo get_the_post_thumbnail($banner->ID,'post-thumbnail',['class' => 'img-responsive responsive--full', 'title' => 'Feature image']); ?>
    <div class="container">
        <h2><?=$banner->post_title; ?></h2>
        <div class="highlight">
            <p class="lead"><?=$banner->post_content;?></p>
            <a class="button" href="<?=get_post_meta( $banner->ID, 'url',true );?>">Learn More ></a>
        </div>
    </div>

<?php endforeach; ?>
</div> 