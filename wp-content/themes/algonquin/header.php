<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 
    <!--link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" /-->
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/stylesheets/style.css">
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/assets/css/lightbox.min.css">

  <!-- or -->

    
  <script src="<?=get_template_directory_uri()?>/assets/js/lightbox/lightbox-plus-jquery.js"></script>
  <script src="<?=get_template_directory_uri()?>/assets/js/scripts.js"></script>
  <script>

</script>
  <?php wp_head(); ?>


</head>
<body <?php body_class(); ?>>
<div id="gabarito"></div>

<div id="" class="hfeed">

    <header id="header" role="banner">
        <nav id="menu" role="navigation">
            <!-- <div id="search">
                <?php get_search_form(); ?>
            </div> -->
            <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
        </nav>
        <section id="branding">
            <div id="site-title">
                <?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { 
                    echo '<h1>'; } ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
                <?php echo esc_html( get_bloginfo( 'name' ) ); ?></a>
                <?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } ?>
            </div>
            <div id="site-description"><?php bloginfo( 'description' ); ?></div>
            <div class="container">
                <img class="logo" src="<?=get_template_directory_uri()?>/assets/images/logo.png" alt="Algoquin College - Student Support" title="Algoquin College - Student Support">
            </div>
        </section>
        
        <?php get_template_part( 'banner' ); ?>
    </header>

    <div id="container">
        <div class="container_wrapper"> 