$(document).ready(function(){
    //initializing
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    })
    
    //adding lightbox
    $(".gallery-item a").attr("data-lightbox", "image-1");

    $("input[name='your-name']").attr("placeholder", "Name");
    $("input[name='your-email']").attr("placeholder", "Email");
    $("textarea[name='your-message']").attr("placeholder", "Comments:");
});