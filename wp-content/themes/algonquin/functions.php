<?php



add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
);
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'blankslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function blankslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}




function mailtrap($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = 'smtp.mailtrap.io';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = 2525;
    $phpmailer->Username = '3d8b1c2cac89c5';
    $phpmailer->Password = '862ea84e172bd8';
  }
  
  add_action('phpmailer_init', 'mailtrap');




  function banners(){
    
        $labels = array(
            'name'               => _x( 'Banners', 'post type general name', 'your-plugin-textdomain' ),
            'singular_name'      => _x( 'Banner', 'post type singular name', 'your-plugin-textdomain' ),
            'menu_name'          => _x( 'Banners', 'admin menu', 'your-plugin-textdomain' ),
            'name_admin_bar'     => _x( 'Banner', 'add new on admin bar', 'your-plugin-textdomain' ),
            'add_new'            => _x( 'Add New', 'Banner', 'your-plugin-textdomain' ),
            'add_new_item'       => __( 'Add New Banner', 'your-plugin-textdomain' ),
            'new_item'           => __( 'New Banner', 'your-plugin-textdomain' ),
            'edit_item'          => __( 'Edit Banner', 'your-plugin-textdomain' ),
            'view_item'          => __( 'View Banner', 'your-plugin-textdomain' ),
            'all_items'          => __( 'All Banners', 'your-plugin-textdomain' ),
            'search_items'       => __( 'Search Banners', 'your-plugin-textdomain' ),
            'parent_item_colon'  => __( 'Parent Banners:', 'your-plugin-textdomain' ),
            'not_found'          => __( 'No Banners found.', 'your-plugin-textdomain' ),
            'not_found_in_trash' => __( 'No Banners found in Trash.', 'your-plugin-textdomain' )
        );
    
        $rewrite = array(
            'slug'                       => 'banners',
            'with_front'                 => true,
            'hierarchical'               => true,
        );
     
        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Description.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => $rewrite,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => true,
            'supports'           => array( 'title', 'editor', 'thumbnail','custom-fields' )
        );
    
        register_post_type( 'banner', $args );   
        register_taxonomy_for_object_type( 'category', 'banner' );
    
}
        
add_action('init', 'banners');