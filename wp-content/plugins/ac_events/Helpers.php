<?php


class Helpers{

    public static function truncate($text, $chars = 225){
        $text = $text." ";
        $text = substr($text,0,$chars);
        $text = substr($text,0,strrpos($text,' '));
        $text = $text."...";
        return $text;
    }

}