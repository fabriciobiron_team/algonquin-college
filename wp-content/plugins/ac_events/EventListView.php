<?php
    $events = EventListController::get();
?>

<?php if($events): ?>
    <?php foreach($events as $event): ?>

        <h3>
            <a href="<?=$event->guid?>">
                <?=$event->post_title;?>
            </a>
        </h3>
        <strong>Starts at:</strong> <time datetime="<?=$event->start_date;?>"><?=$event->start_date;?></time><br/>
        <strong>Finishes at:</strong> <time datetime="<?=$event->end_date;?>"><?=$event->end_date;?></time>
        <p><?=Helpers::truncate($event->post_content);?></p>

    <?php endforeach;?>
<?php else: ?>

    <h5>There are no events scheduled.</h5>

<?php endif; ?>