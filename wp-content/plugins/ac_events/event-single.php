<?php 
// require 'carbon.php';
use Carbon\Carbon;
?>
<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header>

<?php if ( is_singular() ) { echo '<h1 class="entry-title">'; } else { echo '<h2 class="entry-title">'; } ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a><?php if ( is_singular() ) { echo '</h1>'; } else { echo '</h2>'; } ?> 
<div class="event_date">

<p><strong>Starts at:</strong> <time datetime="<?=get_post_meta( $v->ID, 'start_date',true );?>"><?=Carbon::parse(get_post_meta( $v->ID, 'start_date',true ))->toDayDateTimeString();?></time></p>
<p><strong>Finishes at:</strong> <time datetime="<?=get_post_meta( $v->ID, 'end_date',true );?>"><?=Carbon::parse(get_post_meta( $v->ID, 'start_date',true ))->toDayDateTimeString();?></time></p>
</div> 
</header>
<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
<?php if ( !is_search() ) get_template_part( 'entry-footer' ); ?>
</article>

<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
<footer class="footer">
<?php get_template_part( 'nav', 'below-single' ); ?>
</footer>

<?php edit_post_link(); ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?> 