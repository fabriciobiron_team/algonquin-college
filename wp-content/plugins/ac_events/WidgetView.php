<?php
    $upcoming = UpcomingController::get();
    $all_events_page_id = get_option('ac_events_plugin_page_id');     
?>

<li class="widget-container">

    <h3 class="widget-title">Upcoming Events</h3>
    <ul class="widget-events">
    <?php if($upcoming): ?>
        <?php foreach($upcoming as $event): ?>
            <li >
            <h4>
                <a href="<?=$event->guid?>">
                    <?=$event->post_title;?>
                </a>
            </h4>
            
            <p><strong>Starts at:</strong> <time datetime="<?=$event->start_date;?>"><?=$event->start_date;?></time></p>
            <p><strong>Finishes at:</strong> <time datetime="<?=$event->end_date;?>"><?=$event->end_date;?></time></p>
            <li>
    <?php endforeach;?>
    <?php else: ?>

        <strong>There are no events scheduled.</strong> 

    <?php endif; ?>    
    </ul>
    <a class="allevents" href="<?=get_page_link($all_events_page_id);?>">All Events</a>

</li>