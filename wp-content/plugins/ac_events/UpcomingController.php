<?php

use Carbon\Carbon;

/**
 * This is a controller used in the EventListView.php
 */
class UpcomingController{

    /**
     * This function gets events since now
     * @return object Post and meta tags
     */
    public static function get(){

        $date_now = Carbon::now('America/Toronto');

        $args = array(
            'posts_per_page'   => -1,
            'offset'           => 0,
            'order'            => 'DESC',
            'meta_key'			=> 'start_date',
            'post_type'        => 'event',
            'post_status'      => 'publish',
            'suppress_filters' => true,
            'orderby'			=> 'meta_value',
            'meta_query'	=> array(
                    'relation'		=> 'AND',
                    array(
                        'key'			=> 'end_date',
                        'compare'		=> '>=',
                        'value'			=> $date_now,
                        'type'			=> 'DATETIME'
                    )
                )
        );

        $posts = get_posts( $args );
        
        $postMap = array_map(function($v) use($date_now){
            return (object)[
                "ID"            =>  $v->ID,
                "post_title"    =>  $v->post_title,
                "post_content"  =>  $v->post_content,
                "start_date"    =>  Carbon::parse(get_post_meta( $v->ID, 'start_date',true ))->toDayDateTimeString(),
                "end_date"      =>  Carbon::parse(get_post_meta( $v->ID, 'end_date',true ))->toDayDateTimeString(),
                "now"           => $date_now,
                "guid"          => $v->guid
                            
            ];    
        },$posts);

        return $postMap;

    }
    
}


